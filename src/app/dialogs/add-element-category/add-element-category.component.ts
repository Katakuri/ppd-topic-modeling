import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, Validators, FormControl} from '@angular/forms';


@Component({
  selector: 'app-add-element-category',
  templateUrl: './add-element-category.component.html',
  styleUrls: ['./add-element-category.component.scss']
})
export class AddElementCategoryComponent implements OnInit {

  form : FormGroup;


  constructor(
    public dialogRef: MatDialogRef<AddElementCategoryComponent>

  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      nameElement : new FormControl('',[Validators.required, Validators.maxLength(15)]),
    });
  }

  closeDialog(){
    this.dialogRef.close();
  }

}
