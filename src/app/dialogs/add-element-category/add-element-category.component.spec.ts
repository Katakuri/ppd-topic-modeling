import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddElementCategoryComponent } from './add-element-category.component';

describe('AddElementCategoryComponent', () => {
  let component: AddElementCategoryComponent;
  let fixture: ComponentFixture<AddElementCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddElementCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddElementCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
