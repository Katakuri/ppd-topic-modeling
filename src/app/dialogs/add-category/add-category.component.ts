import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, Validators, FormControl} from '@angular/forms';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit {

  form : FormGroup;

  constructor(
    public dialogRef: MatDialogRef<AddCategoryComponent>
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      nameCategory : new FormControl('',[Validators.required, Validators.maxLength(15), Validators.minLength(5)]),
    });
  }

  closeDialog(){
    this.dialogRef.close();
  }

}
