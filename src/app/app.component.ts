import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { AddCategoryComponent } from "../app/dialogs/add-category/add-category.component";
import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DeleteCategoryComponent } from '../app/dialogs/delete-category/delete-category.component'
import { AddElementCategoryComponent } from '../app/dialogs/add-element-category/add-element-category.component'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(public dialog: MatDialog) { }

  listColums = [
    {
      name: 'Sports',
      value: [
        'Football',
        'Basketball',
        'MMA',
        'Tennis',
        'Judo'
      ]
    },
    {
      name: 'Langues',
      value: [
        'Français',
        'Anglais',
        'Espagnol',
        'Portugais',
        'Arabe',
        'Japonais'
      ]
    },
    {
      name: 'IT',
      value: [
        'Web',
        'Machine Learning',
        'Internet',
        'Big Data',
      ]
    }
  ]


  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddCategoryComponent, {
      width: 'auto',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.nameCategory.length > 0) {
        this.listColums.push({ name: result.nameCategory, value: [] });
      }
    });
  }

  addElementToCategory(index) {
    const dialogRef = this.dialog.open(AddElementCategoryComponent, {
      width: 'auto'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.nameElement.length > 0) {
        this.listColums[index].value.push(result.nameElement);
      }
    })
  }

  removeFromArray(index) {
    this.listColums.splice(index, 1);
  }


  deleteElementCategory(index,ind){
    console.log(index +' '+ ind);
    this.listColums[index].value.splice(ind,1);

  }

  deleteCategory(category, index) {
    const dialogRef = this.dialog.open(DeleteCategoryComponent, {
      width: 'auto',
      data: {
        item: category
      }
    }).afterClosed().subscribe((res) => {
      if(res){
        this.removeFromArray(index);
      }
    });
  }
}
