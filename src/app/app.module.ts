import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {DemoMaterialModule} from './material-module';
import { AddCategoryComponent } from './dialogs/add-category/add-category.component';
import {ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { DeleteCategoryComponent } from './dialogs/delete-category/delete-category.component';
import { AddElementCategoryComponent } from './dialogs/add-element-category/add-element-category.component';

@NgModule({
  declarations: [
    AppComponent,
    AddCategoryComponent,
    HeaderComponent,
    FooterComponent,
    DeleteCategoryComponent,
    AddElementCategoryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DemoMaterialModule,
    ReactiveFormsModule,
    FormsModule
  ],
  entryComponents:[AddCategoryComponent,DeleteCategoryComponent,AddElementCategoryComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
